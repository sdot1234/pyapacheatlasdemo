import json
from html import entities
from pyapacheatlas.auth import ServicePrincipalAuthentication
from pyapacheatlas.core import PurviewClient

import config

auth = ServicePrincipalAuthentication(
    tenant_id = config.tenant_id, 
    client_id = config.client_id, 
    client_secret = config.client_secret
)

# Create a client to connect to your service.
client = PurviewClient(
    account_name = config.account_name,
    authentication = auth
)

from pyapacheatlas.readers import ExcelConfiguration, ExcelReader

ec = ExcelConfiguration()
reader = ExcelReader(ec)

entities = reader.parse_bulk_entities("./demo.xlsx")

results = client.upload_entities(entities)
